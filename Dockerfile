# Base Image
FROM debian:bullseye-slim

# Install 
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

# Copy web service files
COPY prayer_time.py /home/jadwal-sholat/prayer_time.py
COPY requirements.txt /home/jadwal-sholat/requirements.txt

# Install Python dependencies
RUN pip3 install --no-cache-dir -r /home/jadwal-sholat/requirements.txt

# Set working directory
WORKDIR /home/jadwal-sholat

# Expose port 
EXPOSE 25043

# Start web service 
CMD ["streamlit", "run", "--server.port=25043", "prayer_time.py" ]
