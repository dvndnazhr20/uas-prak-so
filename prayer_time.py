import streamlit as st
import requests

def get_prayer_times(city):
    url = "https://api.aladhan.com/v1/timingsByCity"
    city_value = city.split(",")[0]
    country = city.split(",")[1]

    params = {
        "city": city_value,
        "country": country,
        "method": 8
    }

    response = requests.get(url, params=params)
    data = response.json()

    prayer_times = data["data"]["timings"]
    date = data["data"]["date"]["readable"]

    return prayer_times, date

def main():
    st.title("Prayer Time")

    city = st.selectbox("Select City:", [
        "Jakarta, Indonesia",
        "London, United Kingdom",
        "New York, United States"
        # Add more city options as needed
    ])

    if st.button("Get Prayer Times"):
        prayer_times, date = get_prayer_times(city)

        st.write(f"City: {city}")
        st.write(f"Date: {date}")

        st.table(prayer_times)

if __name__ == "_main_":
    main()
