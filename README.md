## UAS PRAK SO

## JADWAL-SHOLAT / PRAYER TIME APPLICATION

Sebuah aplikasi web sederhana yang menggunakan Streamlit sebagai framework untuk membuat antarmuka pengguna (user interface) dan menggunakan requests untuk melakukan permintaan HTTP ke API.

![PRAYER_TIME](https://gitlab.com/dvndnazhr20/uas-prak-so/-/raw/main/ss/PRAYER_TIME_1.png)

## DESKRIPSI PROJECT

Aplikasi ini bertujuan untuk memberikan informasi waktu-waktu salat bagi pengguna berdasarkan kota yang dipilih. Pengguna dapat memilih kota dari daftar yang disediakan, dan setelah memilih, aplikasi akan mengambil data waktu-waktu salat dari API Aladhan menggunakan permintaan HTTP. Data tersebut akan ditampilkan dalam bentuk tabel di antarmuka pengguna.

![PRAYER_TIME](https://gitlab.com/dvndnazhr20/uas-prak-so/-/raw/main/ss/PRAYER_TIME_2.png)

## PENJELASAN BAGAIMANA SISTEM OPERASI DALAM PROSES CONTAINERIZATION

Sistem Operasi dalam proses containerization berperan dalam menyediakan lingkungan dan sumber daya yang diperlukan untuk menjalankan kontainer. Kontainer berjalan di atas sistem operasi tuan rumah dan menggunakan kernel tuan rumah untuk mengakses sumber daya seperti CPU, memori, jaringan, dan sistem berkas. Sistem operasi tuan rumah juga memberikan isolasi antara kontainer, memastikan bahwa mereka tidak saling mempengaruhi, dan mengatur pembagian sumber daya yang adil di antara kontainer. Dengan menggunakan sistem operasi yang tepat dan alat containerization seperti Docker, aplikasi dapat diemas dalam unit terisolasi yang disebut kontainer, yang memungkinkan portabilitas dan efisiensi dalam pengembangan, penyebaran, dan menjalankan aplikasi.

## PENJELASAN BAGAIMANA CONTAINERIZATION DAPAT MEMBANTU MEMPERMUDAH PENGEMBANGAN APLIKASI

Containerization membantu mempermudah pengembangan aplikasi dengan menyediakan isolasi lingkungan, portabilitas, pengelolaan dependensi yang jelas, skalabilitas, dan kemampuan untuk mereproduksi dan menguji aplikasi dengan konsisten. Ini membantu mengurangi kompleksitas pengembangan, meningkatkan efisiensi, dan mempercepat siklus pengembangan aplikasi.

## PENJELASAN APA ITU DEVOPS, DAN BAGAIMANA DEVOPS MEMBANTU PENGEMBANGAN APLIKASI

DevOps adalah pendekatan yang menggabungkan pengembangan perangkat lunak (development) dan operasi teknologi informasi (operations) untuk meningkatkan efisiensi dan kecepatan pengembangan aplikasi serta pengiriman produk ke pasar. DevOps melibatkan kolaborasi tim, otomatisasi proses, praktik Continuous Integration dan Continuous Deployment (CI/CD), "infrastruktur sebagai kode", serta monitorisasi dan manajemen kinerja. Dengan DevOps, pengembang dapat merespons perubahan dengan cepat, mempercepat waktu pengembangan, meningkatkan kualitas perangkat lunak, dan memberikan aplikasi yang lebih andal dan inovatif ke pasar.

## CONTOH KASUS PENERAPAN DEVOPS DALAM PERUSAHAAN

Dalam sebuah startup, penerapan DevOps membantu mempercepat pengembangan aplikasi, meningkatkan fleksibilitas dan skalabilitas infrastruktur, mendorong kolaborasi tim yang erat, mengurangi kerugian waktu dan sumber daya, serta memastikan kualitas dan performa aplikasi yang baik.


## DEMO APLIKASI 

 LINK YT : [WEB SEDERHANA](https://youtu.be/oJzjl-tdJvA)
 

## LINK PRAYER_TIME

[Prayer_Time](http://135.181.26.148:25043/)


## LINK DOCKER HUB

[DOCKER HUB](https://hub.docker.com/repository/docker/dvndnazz/jadwal-sholat/general)

![Dockerfile](https://gitlab.com/dvndnazhr20/uas-prak-so/-/raw/main/ss/dockerhub_dockerimages.png)

![Dockerfile](https://gitlab.com/dvndnazhr20/uas-prak-so/-/raw/main/ss/DOCIM.png)


## SS && WEBSERVICE


![Dockerfile](https://gitlab.com/dvndnazhr20/uas-prak-so/-/raw/main/ss/dockerfile.png)

![Docker-Compose](https://gitlab.com/dvndnazhr20/uas-prak-so/-/raw/main/ss/docker-compose.png)


